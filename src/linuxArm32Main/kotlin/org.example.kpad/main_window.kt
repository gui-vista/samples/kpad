@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.kpad

import glib2.gpointer
import gtk3.GtkOrientation
import gtk3.GtkPolicyType
import gtk3.GtkToolButton
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.boxLayout
import io.gitlab.guiVista.gui.widget.display.imageWidget
import io.gitlab.guiVista.gui.widget.display.statusBarWidget
import io.gitlab.guiVista.gui.widget.textEditor.textViewWidget
import io.gitlab.guiVista.gui.widget.tool.item.ToolButton
import io.gitlab.guiVista.gui.widget.tool.item.toolButtonWidget
import io.gitlab.guiVista.gui.widget.tool.toolBarWidget
import io.gitlab.guiVista.gui.window.AppWindow
import io.gitlab.guiVista.gui.window.ScrolledWindow
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction
import org.example.kpad.Controller.iconDir

internal class MainWindow(app: GuiApplication) : AppWindow(app) {
    private val editor by lazy { createEditor() }
    val buffer
        get() = editor.buffer
    private val statusBar by lazy { createStatusBar() }
    private val toolBar by lazy { createToolbar() }

    override fun resetFocus() {
        editor.grabFocus()
    }

    private fun createEditor() = textViewWidget {
        val margins = mapOf("start" to 10, "end" to 10, "top" to 10)
        marginStart = margins["start"] ?: 0
        marginEnd = margins["end"] ?: 0
        marginTop = margins["top"] ?: 0
        cursorVisible = true
    }

    fun updateStatusBar(txt: String) {
        statusBar.push(0u, txt)
    }

    override fun createMainLayout(): ContainerBase = boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_VERTICAL) {
        spacing = 5
        appendChild(createMenuBar())
        appendChild(toolBar)
        appendChild(child = createScrolledWindow(), fill = true, expand = true)
        appendChild(statusBar)
    }

    private fun createScrolledWindow() = ScrolledWindow.create().apply {
        // Using add instead of addChild to avoid the, "Attempting to add a widget with type GtkBox to a
        // GtkApplicationWindow, but as a GtkBin subclass a GtkApplicationWindow can only contain one widget at a
        // time" warning.
        add(editor)
        // Disable the infamous Overlay scrollbars.
        overlayScrolling = false
        // Always show vertical, and horizontal scrollbars.
        changePolicy(GtkPolicyType.GTK_POLICY_ALWAYS, GtkPolicyType.GTK_POLICY_ALWAYS)
    }

    private fun createStatusBar() = statusBarWidget { push(0u, "Ready") }

    private fun createToolbar() = toolBarWidget {
        val newItem = toolButtonWidget(label = "New",
            iconWidget = imageWidget { changeFile("$iconDir/new_document_16.png") }) {
            tooltipText = "New Document"
        }
        val openItem = toolButtonWidget(label = "Open",
            iconWidget = imageWidget { changeFile("$iconDir/open_document_16.png") }) {
            tooltipText = "Open Document"
        }
        val saveItem = toolButtonWidget(label = "Save",
            iconWidget = imageWidget { changeFile("$iconDir/save_document_16.png") }) {
            tooltipText = "Save Document"
        }
        val toolItems = arrayOf(newItem, openItem, saveItem)

        setupToolButtonEvents(openItem, newItem, saveItem)
        toolItems.forEach { insert(it, -1) }
    }

    private fun setupToolButtonEvents(openItem: ToolButton, newItem: ToolButton, saveItem: ToolButton) {
        openItem.connectClickedEvent(staticCFunction(::openItemClicked))
        newItem.connectClickedEvent(staticCFunction(::newItemClicked))
        saveItem.connectClickedEvent(staticCFunction(::saveItemClicked))
    }
}

@Suppress("UNUSED_PARAMETER")
private fun saveItemClicked(
    toolBtn: CPointer<GtkToolButton>,
    userData: gpointer
) {
    Controller.saveFile()
}

@Suppress("UNUSED_PARAMETER")
private fun openItemClicked(
    toolBtn: CPointer<GtkToolButton>,
    userData: gpointer
) {
    Controller.openFile()
}

@Suppress("UNUSED_PARAMETER")
private fun newItemClicked(
    toolBtn: CPointer<GtkToolButton>,
    userData: gpointer
) {
    Controller.newFile()
}
