package org.example.kpad

import glib2.gpointer
import gtk3.GtkMenuItem
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.menu.item.menuItem
import io.gitlab.guiVista.gui.widget.menu.menuBarWidget
import io.gitlab.guiVista.gui.widget.menu.menuWidget

private val newItem by lazy {
    menuItem(mnemonic = "_New") {
        connectActivateEvent(staticCFunction(::newItemActivated), fetchEmptyDataPointer())
    }
}
private val openItem by lazy {
    menuItem(mnemonic = "_Open") {
        connectActivateEvent(staticCFunction(::openItemActivated), fetchEmptyDataPointer())
    }
}
private val saveItem by lazy {
    menuItem(mnemonic = "_Save") {
        connectActivateEvent(staticCFunction(::saveItemActivated), fetchEmptyDataPointer())
    }
}
private val quitItem by lazy {
    menuItem(mnemonic = "_Quit") {
        connectActivateEvent(staticCFunction(::quitItemActivated), fetchEmptyDataPointer())
    }
}
private val aboutItem by lazy {
    menuItem(mnemonic = "_About") {
        connectActivateEvent(staticCFunction(::aboutItemActivated), fetchEmptyDataPointer())
    }
}

private fun createFileMenu() = menuWidget {
    append(newItem)
    append(openItem)
    append(saveItem)
    append(quitItem)
}

private fun createHelpMenu() = menuWidget { add(aboutItem) }

internal fun createMenuBar() = menuBarWidget {
    val fileItem = menuItem(mnemonic = "_File") {
        subMenu = createFileMenu()
    }
    val helpItem = menuItem(mnemonic = "_Help") {
        subMenu = createHelpMenu()
    }
    append(fileItem)
    append(helpItem)
}

@Suppress("UNUSED_PARAMETER")
private fun aboutItemActivated(menuItem: CPointer<GtkMenuItem>, userData: gpointer) {
    Controller.showAboutDialog()
}


@Suppress("UNUSED_PARAMETER")
private fun quitItemActivated(menuItem: CPointer<GtkMenuItem>, userData: gpointer) {
    Controller.exitProgram()
}

@Suppress("UNUSED_PARAMETER")
private fun newItemActivated(menuItem: CPointer<GtkMenuItem>, userData: gpointer) {
    Controller.newFile()
}

@Suppress("UNUSED_PARAMETER")
private fun openItemActivated(menuItem: CPointer<GtkMenuItem>, userData: gpointer) {
    Controller.openFile()
}

@Suppress("UNUSED_PARAMETER")
private fun saveItemActivated(menuItem: CPointer<GtkMenuItem>, userData: gpointer) {
    Controller.saveFile()
}
